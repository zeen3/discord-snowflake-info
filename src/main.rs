use std::{env::args, fmt::{self, Write as _}, num::NonZeroU64 as U64};
struct Snowflake {
    raw: U64,
}
impl Snowflake {
    pub fn new(raw: U64) -> Self {
        Self { raw }
    }
    pub fn new_u64(raw: u64) -> Option<Self> {
        U64::new(raw).map(Self::new)
    }
}
struct Display<T: fmt::Display>(T);
impl<T: fmt::Display> fmt::Debug for Display<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}
struct StringDebug<T>(T);
impl<T: fmt::Debug> fmt::Debug for StringDebug<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_char('"')
            .and_then(|()| fmt::Debug::fmt(&self.0, f))
            .and_then(|()| f.write_char('"'))
    }
}
impl fmt::Debug for Snowflake {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use chrono::*;
        let raw = self.raw.get();
        let offset = (raw >> 22) as i64;
        let offset = Duration::milliseconds(offset);
        let epoch = Utc.ymd(2015, 1, 1).and_hms(0, 0, 0);
        let date = epoch + offset;

        f.debug_struct("Snowflake")
            .field("raw", &self.raw)
            .field("timestamp", &StringDebug(date))
            .field("offset", &StringDebug(Display(offset)))
            .field("worker_id", &((raw & 0x3E0000) >> 17))
            .field("process_id", &((raw & 0x1F000) >> 12))
            .field("increment", &(raw & 0xFFF))
            .finish()
    }
}

fn main() {
    let res = args()
        .skip(1)
        .map(|v| v.parse().map(Snowflake::new).map_err(|err| (err, v)));
    for flake in res {
        #[cfg(feature = "result")]
        {
            println!("{:#?}", flake);
        }
        #[cfg(not(feature = "result"))]
        {
            if let Ok(f) = flake {
                println!("{:#?}", f)
            }
        }
    }
}
